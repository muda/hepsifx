﻿using System;
using HepsiFx.UI.Util;
using Microsoft.Extensions.DependencyInjection;

namespace HepsiFx.UI
{
    class Program
    {
        static void Main(string[] args)
        {
            IServiceCollection serviceCollection = new ServiceCollection();
            var serviceProvider = DependencyManager.Init(serviceCollection);

            var commandLineExecutor = serviceProvider.GetService<ICommandLineExecutor>();

            while (true)
            {
                var arguments = Console.ReadLine();
                if(string.IsNullOrEmpty(arguments) || arguments.Equals("break"))
                    break;

               Console.WriteLine(commandLineExecutor.Execute(arguments));
            }

            Console.Read();
        }
    }
}
