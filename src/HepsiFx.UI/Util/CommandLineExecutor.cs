﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using EnsureThat;
using HepsiFx.ApplicationServices.Command;
using HepsiFx.Command;
using HepsiFx.UI.CommandProcessors;

namespace HepsiFx.UI.Util
{
    public class CommandLineExecutor : ICommandLineExecutor
    {
        private readonly ICommandBus _commandBus;

        private Dictionary<string, object> _commandList = new Dictionary<string, object>();

        public CommandLineExecutor(ICommandBus commandBus)
        {
            _commandBus = commandBus;
            _commandList.Add("get_campaign_info", new GetCampaignInfoProcessor(_commandBus));
            _commandList.Add("create_product", new CreateProductProcessor(_commandBus));
            _commandList.Add("get_product_info", new GetProductInfoProcessor(_commandBus));
            _commandList.Add("create_order", new CreateOrderProcessor(_commandBus));
            _commandList.Add("create_campaign", new CreateCampaignProcessor(_commandBus));
            _commandList.Add("increase_time", new IncreaseTimeProcessor(_commandBus));

        }
        public string Execute(string args)
        {
            if (string.IsNullOrEmpty(args))
                throw new ArgumentNullException();

            var commandLineParams = args.Split(" ", StringSplitOptions.RemoveEmptyEntries).ToList();

            var firstArgument = commandLineParams[0];
            commandLineParams.RemoveAt(0);

            var commandProcessor = _commandList[firstArgument] as ICommandProcessor;
            if (commandProcessor != null)
                return commandProcessor.Process(commandLineParams);

            throw new ArgumentOutOfRangeException();
        }
    }
}