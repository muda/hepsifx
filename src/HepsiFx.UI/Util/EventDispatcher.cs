﻿using System.Collections.Generic;
using System.Linq;
using Autofac;
using HepsiFx.Core;

namespace HepsiFx.UI.Util
{
    public class EventDispatcher : IEventDispatcher
    {
        private readonly ILifetimeScope _container;

        public EventDispatcher(ILifetimeScope container)
        {
            _container = container;
        }

        public void Dispatch<TEvent>(TEvent eventToDispatch) where TEvent : IDomainEvent
        {
            var handlers = _container.Resolve<IEnumerable<IDomainEventHandler<TEvent>>>().ToList();
            handlers.ForEach(handler => handler.Handle(eventToDispatch));
        }
    }
}
