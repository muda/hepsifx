﻿using System;
using System.Reflection;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using HepsiFx.ApplicationServices.Command;
using HepsiFx.ApplicationServices.CommandHandlers;
using HepsiFx.ApplicationServices.EventHandlers;
using HepsiFx.Command;
using HepsiFx.Core;
using HepsiFx.Data;
using HepsiFx.Domain;
using HepsiFx.Events;
using HepsiFx.Factories;
using HepsiFx.Repositories;
using HepsiFx.UI.Util;
using Microsoft.Extensions.DependencyInjection;

namespace HepsiFx.UI
{
    public class DependencyManager
    {
        public static IServiceProvider Init(IServiceCollection collection)
        {
            var containerBuilder = new ContainerBuilder();
            containerBuilder.Populate(collection);

            RegisterServices(containerBuilder);

            var container = containerBuilder.Build();
            var serviceProvider = new AutofacServiceProvider(container);

            using (var scope = container.BeginLifetimeScope())
            {
                ContainerBuilder updater = new ContainerBuilder();

                var commands = CommandRegistrationTable.Global;
                commands.CommandHandlerResolver = type => serviceProvider.GetService(type) as ICommandHandler;
                commands.RegisterFromAssemblyOfType<GetCampaignInfoCommandHandler>();

                updater.Update(scope.ComponentRegistry);
            }

            var dispatcher = serviceProvider.GetService<IEventDispatcher>();
            DomainEvents.Dispatcher = dispatcher;
            return serviceProvider;
        }

        private static void RegisterServices(ContainerBuilder containerBuilder)
        {
            containerBuilder.RegisterType<DateTimeProviderFactory>().As<IDateTimeProviderFactory>();
            containerBuilder.RegisterType<CommandLineExecutor>().As<ICommandLineExecutor>();
            containerBuilder.RegisterType<InMemoryOrderRepository>().As<IOrderRepository>().SingleInstance();
            containerBuilder.RegisterType<InMemoryCampaignRepository>().As<ICampaignRepository>().SingleInstance();
            containerBuilder.RegisterType<InMemoryProductRepository>().As<IProductRepository>().SingleInstance();
            containerBuilder.RegisterType<PriceCalculationStrategy>().As<IPriceCalculationStrategy>();

            containerBuilder.RegisterType<CampaignStartedEventHandler>()
                .As<IDomainEventHandler<CampaignStartedEvent>>();
            containerBuilder.RegisterType<OrderCreatedEventHandler>()
                .As<IDomainEventHandler<OrderCreatedEvent>>();

            containerBuilder.RegisterType<InMemoryCommandBus>().As<ICommandBus>();
            containerBuilder.RegisterAssemblyTypes(Assembly.GetAssembly(typeof(CreateCampaignCommandHandler))).Where(x => x.Name.EndsWith("CommandHandler") || x.Name.EndsWith("Command"));

            containerBuilder.RegisterType<EventDispatcher>().As<IEventDispatcher>();
        }
    }
}
