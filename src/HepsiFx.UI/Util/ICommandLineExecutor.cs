﻿namespace HepsiFx.UI.Util
{
    public interface ICommandLineExecutor
    {
        string Execute(string args);
    }
}
