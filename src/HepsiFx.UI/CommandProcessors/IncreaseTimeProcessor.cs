﻿using System;
using System.Collections.Generic;
using System.Text;
using HepsiFx.ApplicationServices.Command;
using HepsiFx.Command;
using HepsiFx.Domain;

namespace HepsiFx.UI.CommandProcessors
{
   public class IncreaseTimeProcessor:ICommandProcessor
   {
       private readonly ICommandBus _commandBus;

       public IncreaseTimeProcessor(ICommandBus commandBus)
       {
           _commandBus = commandBus;
       }

       public string Process(List<string> arguments)
       {
           var increasedTimeCommand = new IncreaseTimeCommand();
           increasedTimeCommand.CampaignId= new Guid(arguments[0]);
           increasedTimeCommand.IncreaseTime = Convert.ToInt32(arguments[1]);

           var campaign = _commandBus.Execute<Campaign>(increasedTimeCommand);
           return $"Time is : {campaign.EndTime.Hour}";
       }
    }
}
