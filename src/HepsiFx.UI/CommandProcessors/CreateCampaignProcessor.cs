﻿using System;
using System.Collections.Generic;
using HepsiFx.ApplicationServices.Command;
using HepsiFx.Command;
using HepsiFx.Domain;

namespace HepsiFx.UI.CommandProcessors
{
    public class CreateCampaignProcessor : ICommandProcessor
    {
        private readonly ICommandBus _commandBus;

        public CreateCampaignProcessor(ICommandBus commandBus)
        {
            _commandBus = commandBus;
        }

        public string Process(List<string> arguments)
        {
            var createCampaignCommand = new CreateCampaignCommand();
            createCampaignCommand.Name = arguments[0];
            createCampaignCommand.ProductCode = arguments[1];
            createCampaignCommand.Duration = Convert.ToInt32(arguments[2]);
            createCampaignCommand.PriceManipulationLimit = Convert.ToInt32(arguments[3]);
            createCampaignCommand.TargetSalesCount = Convert.ToInt32(arguments[4]);

            var campaign = _commandBus.Execute<Campaign>(createCampaignCommand);
            return campaign != null ? campaign.ToString() : "Campaign cannot created";
        }
    }
}
