﻿using System;
using System.Collections.Generic;
using HepsiFx.ApplicationServices.Command;
using HepsiFx.Command;
using HepsiFx.Domain;

namespace HepsiFx.UI.CommandProcessors
{
    public class CreateOrderProcessor:ICommandProcessor
    {
        private readonly ICommandBus _commandBus;

        public CreateOrderProcessor(ICommandBus commandBus)
        {
            _commandBus = commandBus;
        }

        public string Process(List<string> arguments)
        {
            var createOrderCommand = new CreateOrderCommand();
            createOrderCommand.ProductCode = arguments[0];
            createOrderCommand.Quantity = Convert.ToInt32(arguments[1]);

            var order = _commandBus.Execute<Order>(createOrderCommand);

            return order != null ? order.ToString() : "Order cannot created";
        }
    }
}
