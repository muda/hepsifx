﻿using System;
using System.Collections.Generic;
using System.Linq;
using HepsiFx.ApplicationServices.Command;
using HepsiFx.Command;
using HepsiFx.Domain;

namespace HepsiFx.UI.CommandProcessors
{
    public class GetCampaignInfoProcessor : ICommandProcessor
    {
        private readonly ICommandBus _commandBus;

        public GetCampaignInfoProcessor(ICommandBus commandBus)
        {
            _commandBus = commandBus;
        }

        public string Process(List<string> arguments)
        {
            var getCampaignInfoCommand = new GetCampaignInfoCommand()
            {
                CampaignId = new Guid(arguments.First())
            };

            var campaign = _commandBus.Execute<Campaign>(getCampaignInfoCommand);

            return campaign.ToString();
        }
    }
}