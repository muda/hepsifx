﻿using System.Collections.Generic;
using System.Linq;
using HepsiFx.ApplicationServices.Command;
using HepsiFx.Command;
using HepsiFx.Domain;

namespace HepsiFx.UI.CommandProcessors
{
    public class GetProductInfoProcessor : ICommandProcessor
    {
        private readonly ICommandBus _commandBus;

        public GetProductInfoProcessor(ICommandBus commandBus)
        {
            _commandBus = commandBus;
        }
        public string Process(List<string> arguments)
        {
            var getProductInfoCommand = new GetProductInfoCommand()
            {
                ProductCode = arguments.First()
            };

            var product = _commandBus.Execute<Product>(getProductInfoCommand);

            return product!=null ? product.ToString() : "Product not found";
        }
    }
}
