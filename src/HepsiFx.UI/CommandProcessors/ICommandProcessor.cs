﻿using System.Collections.Generic;

namespace HepsiFx.UI.CommandProcessors
{
    public interface ICommandProcessor
    {
        string Process(List<string> arguments);
    }
}
