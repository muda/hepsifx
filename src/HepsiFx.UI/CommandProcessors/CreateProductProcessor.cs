﻿using System;
using System.Collections.Generic;
using System.Text;
using HepsiFx.ApplicationServices.Command;
using HepsiFx.Command;
using HepsiFx.Domain;

namespace HepsiFx.UI.CommandProcessors
{
    public class CreateProductProcessor : ICommandProcessor
    {
        private readonly ICommandBus _commandBus;

        public CreateProductProcessor(ICommandBus commandBus)
        {
            _commandBus = commandBus;
        }

        public string Process(List<string> arguments)
        {
            var createProductCommand = new CreateProductCommand();
            createProductCommand.ProductCode = arguments[0];
            createProductCommand.Price = Convert.ToDecimal(arguments[1]);
            createProductCommand.Stock = Convert.ToInt32(arguments[2]);

            var product = _commandBus.Execute<Product>(createProductCommand);

            return product != null ? product.ToString() : "Product cannot created";
        }
    }
}
