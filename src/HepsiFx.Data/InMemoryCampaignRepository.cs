﻿using System.Collections.Generic;
using System.Linq;
using EnsureThat;
using HepsiFx.Domain;
using HepsiFx.Repositories;

namespace HepsiFx.Data
{
    public class InMemoryCampaignRepository : ICampaignRepository
    {
        private static List<Campaign> _campaigns = new List<Campaign>();

        public Campaign Find(string productCode)
        {
            Ensure.That(productCode).IsNotNullOrWhiteSpace();

            return _campaigns.FirstOrDefault(x => x.ProductCode.Equals(productCode));
        }

        public Campaign Find(CampaignId campaignId)
        {
            Ensure.That(campaignId).IsNotDefault();

            return _campaigns.FirstOrDefault(x => x.CampaignId.Equals(campaignId));
        }

        public bool SaveCampaign(Campaign campaign)
        {
            Ensure.That(campaign).IsNotNull();

            _campaigns.Add(campaign);

            return true;
        }

        public bool UpdateCampaign(Campaign updateCampaign)
        {
            var currentCampaign = Find(updateCampaign.CampaignId);

            if (currentCampaign == null)
                return false;

            _campaigns.Remove(currentCampaign);
            _campaigns.Add(updateCampaign);

            return true;
        }
    }
}
