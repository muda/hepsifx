﻿using System.Collections.Generic;
using System.Linq;
using EnsureThat;
using HepsiFx.Domain;
using HepsiFx.Repositories;

namespace HepsiFx.Data
{
    public class InMemoryProductRepository : IProductRepository
    {
        private List<Product> _products = new List<Product>();

        public InMemoryProductRepository()
        {
            _products.Add(new Product("HBV00000BJBDV", 1649, 200));
            _products.Add(new Product("OUTFARG2300", 535.42m, 5345));
            _products.Add(new Product("HBV000008C4CY", 1241.10m, 54));
            _products.Add(new Product("OUTHZTG2002", 75, 3455));
            _products.Add(new Product("HBV00000882I4", 63, 23));
            _products.Add(new Product("HBV0000045XI3", 126, 867));
        }
        public bool SaveProduct(Product product)
        {
            Ensure.That(product).IsNotNull();
            Ensure.That(product.ProductCode).IsNotNullOrWhiteSpace();
            Ensure.That(product.Price).IsGt(decimal.Zero);
            Ensure.That(product.Stock).IsGt(0);

            var productExist = Find(product.ProductCode);
            if (productExist != null)
            {
                _products.Remove(productExist);
            }

            _products.Add(product);
            return true;
        }

        public Product Find(string productCode)
        {
            Ensure.That(productCode).IsNotNullOrWhiteSpace();

            return _products.FirstOrDefault(x => x.ProductCode.Equals(productCode));
        }
    }
}
