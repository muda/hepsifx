﻿using System.Collections.Generic;
using EnsureThat;
using HepsiFx.Domain;
using HepsiFx.Repositories;

namespace HepsiFx.Data
{
    public class InMemoryOrderRepository : IOrderRepository
    {
        private static List<Order> _orders = new List<Order>();

        public InMemoryOrderRepository()
        {

        }


        public bool SaveOrder(Order order)
        {
            Ensure.That(order).IsNotNull();
            Ensure.That(order.ProductCode).IsNotNullOrWhiteSpace();
            Ensure.That(order.Quantity).IsGt(0);

            _orders.Add(order);
            return true;
        }
    }
}
