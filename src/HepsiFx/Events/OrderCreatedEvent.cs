﻿using HepsiFx.Core;
using HepsiFx.Domain;

namespace HepsiFx.Events
{
    public class OrderCreatedEvent : IDomainEvent
    {
        public OrderCreatedEvent(Order order)
        {
            Order = order;
        }
        public Order Order { get; private set; }
    }
}
