﻿using HepsiFx.Core;
using HepsiFx.Domain;

namespace HepsiFx.Events
{
    public class CampaignStartedEvent : IDomainEvent
    {
        public CampaignStartedEvent(Campaign campaign)
        {
            Campaign = campaign;
        }
        public Campaign Campaign { get; private set; }
    }
}
