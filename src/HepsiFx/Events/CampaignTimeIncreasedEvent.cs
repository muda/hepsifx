﻿using HepsiFx.Core;
using HepsiFx.Domain;

namespace HepsiFx.Events
{
    public class CampaignTimeIncreasedEvent : IDomainEvent
    {
        public CampaignTimeIncreasedEvent(Campaign campaign, int extraHour)
        {
            Campaign = campaign;
            Hour = extraHour;
        }
        public Campaign Campaign { get; private set; }
        public int Hour { get; private set; }
    }
}
