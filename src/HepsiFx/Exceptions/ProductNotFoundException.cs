﻿using System;
using EnsureThat;

namespace HepsiFx.Exceptions
{
    public class ProductNotFoundException : Exception
    {
        public string ProductCode { get; set; }

        public ProductNotFoundException()
        {
            
        }

        public ProductNotFoundException(string productCode)
        {
            Ensure.That(productCode).IsNotNullOrWhiteSpace();
            this.ProductCode = productCode;
        }
    }
}
