﻿using System;

namespace HepsiFx.Command
{
    public class InMemoryCommandBus : ICommandBus
    {
        public CommandRegistrationTable CommandRegistrations { get; private set; }
       
        public InMemoryCommandBus()
            : this(CommandRegistrationTable.Global)
        {
        }

        public InMemoryCommandBus(CommandRegistrationTable queryRegistrations)
        {
            CommandRegistrations = queryRegistrations;
        }

        public void Dispose()
        {

        }

        public TResult Execute<TResult>(ICommand command)
        {
            Type queryType = command.GetType();
            ICommandHandler commandHandler = CommandRegistrations.GetHandlerForCommand(queryType);
            return (TResult)commandHandler.Execute(command);
        }
    }
}
