﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace HepsiFx.Command
{
    public class CommandRegistrationTable
    {
        public static readonly CommandRegistrationTable Global = new CommandRegistrationTable();


        private readonly Dictionary<CommandHandlerKey, Type> commandHandlers;
        public IReadOnlyDictionary<CommandHandlerKey, Type> CommandHandlers
        {
            get { return this.commandHandlers; }
        }

        public Func<Type, ICommandHandler> CommandHandlerResolver { get; set; }

        public CommandRegistrationTable()
        {
            this.commandHandlers = new Dictionary<CommandHandlerKey, Type>();
           // CommandHandlerResolver = type => Activator.CreateInstance(type) as ICommandHandler;
        }

        public void Add<TCommand, TCommandHandler>(CultureInfo cultureInfo)
            where TCommand : ICommand
            where TCommandHandler : ICommandHandler
        {
            Type commandType = typeof(TCommand);
            Type commandHandlerType = typeof(TCommandHandler);

            Add(commandType, commandHandlerType);
        }

        public void Add(Type commandType, Type commandHandlerType)
        {
            var key = new CommandHandlerKey(commandType);
            if (!this.commandHandlers.ContainsKey(key))
                this.commandHandlers.Add(key, commandHandlerType);
        }

        public ICommandHandler<TCommand> GetHandlerForCommand<TCommand>()
            where TCommand : ICommand
        {
            Type commandType = typeof(TCommand);
            return GetHandlerForCommand(commandType) as ICommandHandler<TCommand>;
        }

        public ICommandHandler GetHandlerForCommand(Type commandType)
        {
            var key = new CommandHandlerKey(commandType);
            var neutralCultureKey = new CommandHandlerKey(commandType);
            CommandHandlerKey keyToCheck = default(CommandHandlerKey);

            if (this.commandHandlers.ContainsKey(key))
                keyToCheck = key;
            if (keyToCheck.Equals(default(CommandHandlerKey)) && this.commandHandlers.ContainsKey(neutralCultureKey))
                keyToCheck = neutralCultureKey;

            if (keyToCheck.Equals(default(CommandHandlerKey)))
                return null;


            var mappingType = this.commandHandlers[keyToCheck];
            var resolvedMapping = CommandHandlerResolver(mappingType);
            return resolvedMapping;
        }

        public void RegisterFromAssemblyOfType<TType>()
        {
            Assembly a = typeof(TType).Assembly;
            var types =
                a.GetTypes()
                    .Where(type =>
                            type != typeof(ICommandHandler) &&
                            typeof(ICommandHandler).IsAssignableFrom(type))
                    .ToList();

            foreach (var type in types)
            {
                var handler = CommandHandlerResolver(type);
                if (handler != null)
                    Add(handler.CommandType, type);
            }
        }
    }
}
