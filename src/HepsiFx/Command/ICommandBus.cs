﻿using System;

namespace HepsiFx.Command
{
    public interface ICommandBus : IDisposable
    {
        CommandRegistrationTable CommandRegistrations { get; }

        TResult Execute<TResult>(ICommand query);
    }
}
