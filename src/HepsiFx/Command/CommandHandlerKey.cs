﻿using System;

namespace HepsiFx.Command
{
    public struct CommandHandlerKey
    {
        public bool Equals(CommandHandlerKey other)
        {
            var isEqual = Equals(Type, other.Type);

            return isEqual;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Type != null ? Type.GetHashCode() : 0) * 397);
            }
        }

        public Type Type { get; set; }

        public CommandHandlerKey(Type type)
            : this()
        {
            Type = type;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is CommandHandlerKey && Equals((CommandHandlerKey)obj);
        }
    }
}
