﻿using System;

namespace HepsiFx.Command
{
    public interface ICommandHandler<in TCommand, TResult> : ICommandHandler
        where TCommand : ICommand
    {
        TResult Execute(TCommand command);
    }

    public interface ICommandHandler<in TCommand> : ICommandHandler
        where TCommand : ICommand
    {
        object Execute(TCommand command);
    }

    public interface ICommandHandler
    {
        Type CommandType { get; }

        object Execute(ICommand command);
    }
}
