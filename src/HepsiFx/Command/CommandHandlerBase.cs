﻿using System;

namespace HepsiFx.Command
{
    public abstract class CommandHandlerBase<TCommand, TResult> : ICommandHandler<TCommand, TResult>
        where TCommand : ICommand
    {
        public abstract TResult Execute(TCommand command);

        public Type CommandType
        {
            get { return typeof(TCommand); }
        }

        public object Execute(ICommand command)
        {
            return Execute((TCommand)command);
        }
    }
}
