﻿using HepsiFx.Domain;

namespace HepsiFx.Factories
{
    public class DateTimeProviderFactory : IDateTimeProviderFactory
    {
        public IDateTimeProvider CreateProvider()
        {
            return new DefaultDateTimeProvider();
        }
    }
}
