﻿using HepsiFx.Domain;

namespace HepsiFx.Factories
{
    public interface IDateTimeProviderFactory
    {
        IDateTimeProvider CreateProvider();
    }
}