﻿using HepsiFx.Domain;

namespace HepsiFx.Repositories
{
    public interface ICampaignRepository
    {
        Campaign Find(CampaignId campaignId);
        Campaign Find(string productCode);
        bool SaveCampaign(Campaign campaign);
        bool UpdateCampaign(Campaign updateCampaign);
    }
}