﻿using HepsiFx.Domain;

namespace HepsiFx.Repositories
{
    public interface IOrderRepository
    {
        bool SaveOrder(Order order);
    }
}
