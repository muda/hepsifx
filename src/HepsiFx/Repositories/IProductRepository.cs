﻿using HepsiFx.Domain;

namespace HepsiFx.Repositories
{
    public interface IProductRepository
    {
        bool SaveProduct(Product product);
        Product Find(string productCode);
    }
}
