﻿using EnsureThat;

namespace HepsiFx.Domain
{
    public class PriceCalculationStrategy : IPriceCalculationStrategy
    {
        private const int PERCENT_VALUE = 100;
        public decimal Calculate(decimal price, decimal calculationLimit)
        {
            Ensure.That(price).IsGt(0);
            Ensure.That(calculationLimit).IsGt(0);

            return price * (PERCENT_VALUE - calculationLimit) / PERCENT_VALUE;
        }
    }
}