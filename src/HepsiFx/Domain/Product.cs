﻿using EnsureThat;
using HepsiFx.Exceptions;

namespace HepsiFx.Domain
{
    public class Product 
    {
        private const int MIN_STOCK_VALUE = 0;

        private string _productCode;
        private decimal _price;
        private int _stock;

        private CampaignId _appliedCampaignId;

        public Product(string productCode, decimal price, int stock)
        {
            Ensure.That(productCode).IsNotNullOrWhiteSpace();
            Ensure.That(price).IsGt(decimal.Zero);
            Ensure.That(stock).IsGt(MIN_STOCK_VALUE);

            this._productCode = productCode;
            this._price = price;
            this._stock = stock;
        }

        public CampaignId AppliedCampaignId => _appliedCampaignId;
        public string ProductCode => _productCode;
        public decimal Price => _price;
        public int Stock => _stock;

        public static Product Null=> new Product("DEFAULT", 0.1m,1);

        public void ApplyCampaign(Campaign campaign, IPriceCalculationStrategy priceCalculationStrategy)
        {
            Ensure.That(campaign).IsNotNull();
            Ensure.That(campaign.PriceManipulationLimit).IsGt(decimal.Zero);

            this._price = priceCalculationStrategy.Calculate(_price, campaign.PriceManipulationLimit);
            _appliedCampaignId = campaign.CampaignId;
        }

        public void Sell(int quantity)
        {
            Ensure.That(quantity).IsGt(0);

            if (quantity > _stock)
                throw new InSufficientStockException();

            this._stock -= quantity;
        }

        public override string ToString()
        {
            return $"Product Code {ProductCode}, Price {Price}, Stock {Stock}";
        }
    }
}
