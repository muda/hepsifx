﻿using HepsiFx.Factories;
using System;
using EnsureThat;
using HepsiFx.Core;
using HepsiFx.Events;

namespace HepsiFx.Domain
{
    public class Campaign
    {
        private CampaignId _campaignId;
        private string _name;
        private string _productCode;
        private int _priceManipulationLimit;
        private int _targetSalesCount;
        private int _usingItemCount;

        public string Name
        {
            get { return _name; }
        }
        public DateTime StartTime { get; private set; }
        public DateTime EndTime { get; private set; }

        public CampaignId CampaignId => _campaignId;
        public string ProductCode
        {
            get { return _productCode; }
        }

        public int TargetSalesCount
        {
            get { return _targetSalesCount; }
        }

        public decimal PriceManipulationLimit
        {
            get { return _priceManipulationLimit; }
        }

        public int UsingItemCount
        {
            get { return _usingItemCount; }
        }

        public Status Status { get; }

        public static Campaign Null
        {
            get
            {
                return new Campaign(0, 0, 0, "", "");
            }
        }

        private readonly IDateTimeProvider _dateTimeProvider;

        private Campaign(int targetSalesCount, int priceManipulationLimit, int duration, string productCode, string name)
        {
            IDateTimeProviderFactory dateTimeProviderFactory = new DateTimeProviderFactory();
            _dateTimeProvider = dateTimeProviderFactory.CreateProvider();

            StartTime = _dateTimeProvider.Now;
            EndTime = StartTime.AddHours(duration);
            _targetSalesCount = targetSalesCount;
            _priceManipulationLimit = priceManipulationLimit;
            _productCode = productCode;
            _name = name;
            _campaignId = CampaignId.New();
        }

        public static Campaign Create(int targetSalesCount, int priceManipulationLimit, int duration, string productCode, string name)
        {
            Ensure.That(targetSalesCount).IsGt(0);
            Ensure.That(priceManipulationLimit).IsGt(0);
            Ensure.That(priceManipulationLimit).IsLt(101);
            Ensure.That(duration).IsGt(0);
            Ensure.That(productCode).IsNotNullOrWhiteSpace();
            Ensure.That(name).IsNotNullOrWhiteSpace();

            var campaign = new Campaign(targetSalesCount, priceManipulationLimit, duration, productCode, name);

            DomainEvents.Raise(new CampaignStartedEvent(campaign));

            return campaign;
        }

        public void Use(int quantity)
        {
            this._usingItemCount += quantity;
        }

        public void IncreaseTime(int hour)
        {
            EndTime = EndTime.AddHours(hour);
        }

        public override string ToString()
        {
            return
                $"Name {Name}, Product {ProductCode}, Duration {EndTime.Hour - StartTime.Hour}, Limit {PriceManipulationLimit}, Target Sales Count {TargetSalesCount}";
        }
    }
}