﻿using System;

namespace HepsiFx.Domain
{
    public interface IDateTimeProvider
    {
        DateTime Now { get;}
    }
}
