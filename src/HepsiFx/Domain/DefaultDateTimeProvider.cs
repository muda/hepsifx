﻿using System;

namespace HepsiFx.Domain
{
    public class DefaultDateTimeProvider : IDateTimeProvider
    {
        public DateTime Now =>  DateTime.Now;
    }
}
