﻿using EnsureThat;
using HepsiFx.Core;
using HepsiFx.Events;

namespace HepsiFx.Domain
{
    public class Order
    {
        public string ProductCode { get; private set; }
        public int Quantity { get; private set; }
        public Order(string productCode, int quantity)
        {
            Ensure.That(productCode).IsNotNullOrWhiteSpace();
            Ensure.That(quantity).IsGt(0);

            ProductCode = productCode;
            Quantity = quantity;

            DomainEvents.Raise(new OrderCreatedEvent(this));
        }

        public static Order Null=> new Order(string.Empty, 0);

        public override string ToString()
        {
            return $"Product Code {ProductCode}, Quantity {Quantity}";
        }
    }
}
