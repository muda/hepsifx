﻿using System;
using EnsureThat;

namespace HepsiFx.Domain
{
    public struct CampaignId
    {
        private Guid value;

        public CampaignId(Guid value)
        {
            Ensure.That(value).IsNotEmpty();
            this.value = value;
        }

        public Guid Value
        {
            get { return this.value; }
            private set { this.value = value; }
        }

        public override bool Equals(object obj)
        {
            var otherId = (CampaignId)obj;
            return otherId.value == this.value;
        }

        public override string ToString()
        {
            return this.value.ToString();
        }

        public static CampaignId New()
        {
            return new CampaignId(Guid.NewGuid());
        }
    }
}
