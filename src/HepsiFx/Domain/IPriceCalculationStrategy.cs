﻿namespace HepsiFx.Domain
{
    public interface IPriceCalculationStrategy
    {
        decimal Calculate(decimal price, decimal calculationLimit);
    }
}
