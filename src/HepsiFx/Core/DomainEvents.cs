﻿namespace HepsiFx.Core
{
    public class DomainEvents
    {
        public static IEventDispatcher Dispatcher { get; set; }

        public static void Raise<TEvent>(TEvent eventToRaise) where TEvent : IDomainEvent
        {
            Dispatcher.Dispatch(eventToRaise);
        }
    }
}




