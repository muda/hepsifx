﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HepsiFx.Core
{
    [AttributeUsage(AttributeTargets.Property)]
    public class OrderAttribute : Attribute
    {
        public int Order { get; set; }
    }
}
