﻿namespace HepsiFx.Core
{
    public interface IEventBus
    {
        //void Register<TDomainEvent, THandler>() where TDomainEvent : IDomainEvent
        //    where THandler : IDomainEventHandler<TDomainEvent>;


        void Raise<T>(T domainEvent) 
            where T : IDomainEvent;
    }
}