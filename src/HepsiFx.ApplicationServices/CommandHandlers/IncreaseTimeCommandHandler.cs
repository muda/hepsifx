﻿using System;
using HepsiFx.ApplicationServices.Command;
using HepsiFx.Command;
using HepsiFx.Domain;
using HepsiFx.Repositories;

namespace HepsiFx.ApplicationServices.CommandHandlers
{
    public class IncreaseTimeCommandHandler : CommandHandlerBase<IncreaseTimeCommand, Campaign>
    {
        private readonly ICampaignRepository _campaignRepository;

        public IncreaseTimeCommandHandler(ICampaignRepository campaignRepository)
        {
            _campaignRepository = campaignRepository;
        }

        public override Campaign Execute(IncreaseTimeCommand command)
        {
            var campaign = _campaignRepository.Find(new CampaignId(command.CampaignId));

            campaign?.IncreaseTime(command.IncreaseTime);

            return campaign;
        }
    }
}
