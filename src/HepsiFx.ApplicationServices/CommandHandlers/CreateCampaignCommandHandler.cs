﻿using System;
using HepsiFx.ApplicationServices.Command;
using HepsiFx.Command;
using HepsiFx.Domain;
using HepsiFx.Repositories;

namespace HepsiFx.ApplicationServices.CommandHandlers
{
    public class CreateCampaignCommandHandler : CommandHandlerBase<CreateCampaignCommand,Campaign>
    {
        private readonly ICampaignRepository _campaignRepository;

        public CreateCampaignCommandHandler(ICampaignRepository campaignRepository)
        {
            _campaignRepository = campaignRepository;
        }

        public override Campaign Execute(CreateCampaignCommand command)
        {
            var campaign = Campaign.Create(command.TargetSalesCount, command.PriceManipulationLimit, command.Duration,
                command.ProductCode, command.Name);

            _campaignRepository.SaveCampaign(campaign);

            return campaign;
        }
    }
}
