﻿using System;
using HepsiFx.ApplicationServices.Command;
using HepsiFx.Command;
using HepsiFx.Domain;
using HepsiFx.Repositories;

namespace HepsiFx.ApplicationServices.CommandHandlers
{
    public class CreateOrderCommandHandler : CommandHandlerBase<CreateOrderCommand, Order>
    {
        private readonly IOrderRepository _orderRepository;

        public CreateOrderCommandHandler(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public override Order Execute(CreateOrderCommand command)
        {
            var order  = new Order(command.ProductCode, command.Quantity);
            _orderRepository.SaveOrder(order);

            return order;
        }
    }
}
