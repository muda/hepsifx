﻿using System;
using HepsiFx.ApplicationServices.Command;
using HepsiFx.Command;
using HepsiFx.Domain;
using HepsiFx.Repositories;

namespace HepsiFx.ApplicationServices.CommandHandlers
{
    public class GetProductInfoCommandHandler : CommandHandlerBase<GetProductInfoCommand, Product>
    {
        private readonly IProductRepository _productRepository;

        public GetProductInfoCommandHandler(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public override Product Execute(GetProductInfoCommand command)
        {
            var product = _productRepository.Find(command.ProductCode);
            return product;
        }
    }
}
