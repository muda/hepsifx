﻿using System;
using HepsiFx.ApplicationServices.Command;
using HepsiFx.Command;
using HepsiFx.Domain;
using HepsiFx.Repositories;

namespace HepsiFx.ApplicationServices.CommandHandlers
{
    public class CreateProductCommandHandler : CommandHandlerBase<CreateProductCommand, Product>
    {
        private readonly IProductRepository _productRepository;

        public CreateProductCommandHandler(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public override Product Execute(CreateProductCommand command)
        {
            var product = new Product(command.ProductCode, command.Price, command.Stock);
            _productRepository.SaveProduct(product);

            return product;
        }
    }
}
