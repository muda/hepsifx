﻿using System;
using HepsiFx.ApplicationServices.Command;
using HepsiFx.Command;
using HepsiFx.Domain;
using HepsiFx.Repositories;

namespace HepsiFx.ApplicationServices.CommandHandlers
{
    public class GetCampaignInfoCommandHandler : CommandHandlerBase<GetCampaignInfoCommand, Campaign>
    {
        private readonly ICampaignRepository _campaignRepository;

        public GetCampaignInfoCommandHandler(ICampaignRepository campaignRepository)
        {
            _campaignRepository = campaignRepository;
        }

        public override Campaign Execute(GetCampaignInfoCommand command)
        {
            var campaign = _campaignRepository.Find(new CampaignId(command.CampaignId));
            return campaign;
        }
    }
}
