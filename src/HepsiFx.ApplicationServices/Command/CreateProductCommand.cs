﻿using HepsiFx.Command;

namespace HepsiFx.ApplicationServices.Command
{
    public class CreateProductCommand : ICommand
    {
        public string ProductCode { get; set; }
        public decimal Price { get; set; }
        public int Stock { get; set; }
    }
}
