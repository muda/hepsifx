﻿using HepsiFx.Command;
using NUnit.Framework.Constraints;

namespace HepsiFx.ApplicationServices.Command
{
    public class CreateCampaignCommand : ICommand
    {
        //int targetSalesCount, int priceManipulationLimit, int duration, string productCode, string name
        public int TargetSalesCount { get; set; }
        public int PriceManipulationLimit { get; set; }
        public int Duration { get; set; }
        public string ProductCode { get; set; }
        public string Name { get; set; }
    }
}