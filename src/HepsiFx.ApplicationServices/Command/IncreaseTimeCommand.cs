﻿using System;
using HepsiFx.Command;

namespace HepsiFx.ApplicationServices.Command
{
    public class IncreaseTimeCommand : ICommand
    {
        public Guid CampaignId { get; set; }
        public int IncreaseTime { get; set; }
    }
}
