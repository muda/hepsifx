﻿using HepsiFx.Command;

namespace HepsiFx.ApplicationServices.Command
{
    public class CreateOrderCommand:ICommand
    {
        public string ProductCode { get; set; }
        public int Quantity { get; set; }
    }
}
