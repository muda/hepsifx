﻿using System;
using HepsiFx.Command;
using HepsiFx.Core;

namespace HepsiFx.ApplicationServices.Command
{
    public class GetProductInfoCommand : ICommand
    {
        [OrderAttribute(Order = 0)]
        public string ProductCode { get; set; }
    }
}
