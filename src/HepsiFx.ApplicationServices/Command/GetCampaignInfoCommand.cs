﻿using System;
using HepsiFx.Command;
using HepsiFx.Core;

namespace HepsiFx.ApplicationServices.Command
{
    public class GetCampaignInfoCommand : ICommand
    {
        [OrderAttribute(Order = 0)]
        public Guid CampaignId { get; set; }
    }
}
