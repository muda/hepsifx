﻿using HepsiFx.Core;
using HepsiFx.Domain;
using HepsiFx.Events;
using HepsiFx.Exceptions;
using HepsiFx.Repositories;

namespace HepsiFx.ApplicationServices.EventHandlers
{
    public class CampaignStartedEventHandler : IDomainEventHandler<CampaignStartedEvent>
    {
        private readonly IProductRepository _productRepository;
        private readonly IPriceCalculationStrategy _priceCalculationStrategy;

        public CampaignStartedEventHandler(IProductRepository productRepository, IPriceCalculationStrategy priceCalculationStrategy)
        {
            _productRepository = productRepository;
            _priceCalculationStrategy = priceCalculationStrategy;
        }

        public void Handle(CampaignStartedEvent @event)
        {
            var product = _productRepository.Find(@event.Campaign.ProductCode);
            if (product == null)
                throw new ProductNotFoundException(@event.Campaign.ProductCode);

            product.ApplyCampaign(@event.Campaign, _priceCalculationStrategy);

            _productRepository.SaveProduct(product);
        }
    }
}