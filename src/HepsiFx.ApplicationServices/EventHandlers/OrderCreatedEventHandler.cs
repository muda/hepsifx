﻿using HepsiFx.Core;
using HepsiFx.Domain;
using HepsiFx.Events;
using HepsiFx.Exceptions;
using HepsiFx.Factories;
using HepsiFx.Repositories;

namespace HepsiFx.ApplicationServices.EventHandlers
{
    public class OrderCreatedEventHandler : IDomainEventHandler<OrderCreatedEvent>
    {
        private readonly IProductRepository _productRepository;
        private readonly ICampaignRepository _campaignRepository;
        private readonly IDateTimeProviderFactory _dateTimeProviderFactory;

        public OrderCreatedEventHandler(IProductRepository productRepository, ICampaignRepository campaignRepository, IDateTimeProviderFactory dateTimeProviderFactory)
        {
            _productRepository = productRepository;
            _campaignRepository = campaignRepository;
            _dateTimeProviderFactory = dateTimeProviderFactory;
        }

        public void Handle(OrderCreatedEvent @event)
        {
            var isAvailableForCampaign = CheckProductCampaignAvailable(@event);
            if (isAvailableForCampaign)
                DecreaseProductStock(@event);
        }

        private bool CheckProductCampaignAvailable(OrderCreatedEvent @event)
        {
            var campaign = _campaignRepository.Find(@event.Order.ProductCode);

            if (campaign == null)
                return true;

            var isSalesCountAvailable = CheckTargetSalesCount(@event, campaign);
            if (!isSalesCountAvailable)
                return false;

            var isCampaignTimeAvailable = CheckCampaignTimeIsAvailable(campaign);
            if (!isCampaignTimeAvailable)
                return false;

            campaign.Use(@event.Order.Quantity);

            _campaignRepository.UpdateCampaign(campaign);

            return true;
        }

        private bool CheckCampaignTimeIsAvailable(Campaign campaign)
        {
            var datetimeProvider = _dateTimeProviderFactory.CreateProvider();
            return campaign.EndTime > datetimeProvider.Now;
        }

        private static bool CheckTargetSalesCount(OrderCreatedEvent @event, Campaign campaign)
        {
            if (campaign.UsingItemCount >= campaign.TargetSalesCount)
                throw new CampaignNotAvailableException();

            var totalSalesCount = campaign.UsingItemCount + @event.Order.Quantity;

            if (totalSalesCount > campaign.TargetSalesCount)
                throw new InsufficientCampaignItemException();

            return true;
        }

        private void DecreaseProductStock(OrderCreatedEvent @event)
        {
            var product = _productRepository.Find(@event.Order.ProductCode);

            if (product == null)
                throw new ProductNotFoundException(@event.Order.ProductCode);

            product.Sell(@event.Order.Quantity);

            _productRepository.SaveProduct(product);
        }
    }
}
