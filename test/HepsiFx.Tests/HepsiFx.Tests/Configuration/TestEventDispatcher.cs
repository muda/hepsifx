﻿using System;
using System.Collections.Generic;
using System.Linq;
using HepsiFx.Core;

namespace HepsiFx.Tests.Configuration
{
    public class TestEventDispatcher : IEventDispatcher
    {
        private static readonly Dictionary<Type, object> Handlers = new Dictionary<Type, object>();

        public void Register<TEvent, TEventHandler>(TEventHandler eventHandler)
            where TEvent : IDomainEvent
            where TEventHandler : IDomainEventHandler<TEvent>
        {
            Handlers.Add(typeof(TEvent), eventHandler);
        }

        public void Clean()
        {
            Handlers?.Clear();
        }

        public void Dispatch<TEvent>(TEvent eventToDispatch) where TEvent : IDomainEvent
        {
            if(!Handlers.Any())
                throw new NotImplementedException();

            var handler = Handlers[eventToDispatch.GetType()] as IDomainEventHandler<TEvent>;

            handler?.Handle(eventToDispatch);
        }
    }
}
