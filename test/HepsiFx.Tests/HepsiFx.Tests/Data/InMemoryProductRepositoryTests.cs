﻿using FluentAssertions;
using HepsiFx.Data;
using HepsiFx.Domain;
using HepsiFx.Repositories;
using NUnit.Framework;

namespace HepsiFx.Tests.Data
{
    [TestFixture]
    public class InMemoryProductRepositoryTests
    {
        private IProductRepository _productRepository;

        [OneTimeSetUp]
        public void Setup()
        {
            _productRepository = new InMemoryProductRepository();
        }


        [Test]
        public void When_Product_Given_Should_Save()
        {
            var savingProduct = new Product("AC42343", 244, 1000);
            _productRepository.SaveProduct(savingProduct);

            var findingProduct = _productRepository.Find(savingProduct.ProductCode);

            findingProduct.Should().NotBeNull();
            findingProduct.Should().Be(savingProduct);
        }
    }
}
