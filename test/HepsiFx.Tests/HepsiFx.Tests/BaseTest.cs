﻿using HepsiFx.ApplicationServices.EventHandlers;
using HepsiFx.Core;
using HepsiFx.Data;
using HepsiFx.Domain;
using HepsiFx.Events;
using HepsiFx.Factories;
using HepsiFx.Tests.Configuration;
using NUnit.Framework;

namespace HepsiFx.Tests
{
    [TestFixture]
    public class BaseTest
    {
        private TestEventDispatcher eventDispatcher = new TestEventDispatcher();
        [SetUp]
        public virtual void Setup()
        {
            eventDispatcher.Register<OrderCreatedEvent, OrderCreatedEventHandler>(new OrderCreatedEventHandler(new InMemoryProductRepository(), new InMemoryCampaignRepository(), new DateTimeProviderFactory()));
            eventDispatcher.Register<CampaignStartedEvent, CampaignStartedEventHandler>(new CampaignStartedEventHandler(new InMemoryProductRepository(), new PriceCalculationStrategy()));

            eventDispatcher.Register<CampaignTimeIncreasedEvent, CampaignTimeIncreasedEventHandler>(new CampaignTimeIncreasedEventHandler());

            DomainEvents.Dispatcher = eventDispatcher;
        }

        [TearDown]
        public virtual void TearDown()
        {
           eventDispatcher.Clean();
        }
    }
}
