﻿using System;
using FluentAssertions;
using HepsiFx.Domain;
using HepsiFx.Exceptions;
using NUnit.Framework;

namespace HepsiFx.Tests.Domain
{
    [TestFixture]
    public class OrderTests : BaseTest
    {

        [Test]
        public void When_ProductCode_Price_Stock_Given_Should_Create_Order()
        {
            var productCode = "HBV00000882I4";
            var quantity = 10;

            var order = new Order(productCode, quantity);

            order.Should().NotBeNull();
            order.ProductCode.Should().Be(productCode);
            order.Quantity.Should().BeGreaterThan(0);
        }

        [TestCase("", 143)]
        [TestCase("HBV00000882I4", 0)]
        public void When_Parameters_Incorrect_Should_Throw_Error(string productCode, int quantity)
        {
            Action act = () => new Order(productCode, quantity);

            act.Should().Throw<ArgumentException>();
        }

        [Test]
        public void When_Product_Stock_Insufficient_Should_Throw_Error()
        {
            var productCode = "HBV00000882I4";
            var quantity = 30;

            Action act = () => new Order(productCode, quantity);

            act.Should().Throw<InSufficientStockException>();
        }
    }
}
