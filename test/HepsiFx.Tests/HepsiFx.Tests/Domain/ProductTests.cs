﻿using FluentAssertions;
using HepsiFx.Domain;
using NUnit.Framework;
using System;
using HepsiFx.Exceptions;

namespace HepsiFx.Tests.Domain
{
    [TestFixture]
    public class ProductTests : BaseTest
    {
        [SetUp]
        public override void Setup()
        {
            base.Setup();
        }

        [Test]
        public void When_ProductCode_Price_Stock_Given_Should_Create_Product()
        {
            var productCode = "OUTHZTG2002";
            var price = 143m;
            var stock = 150;

            var product = new Product(productCode, price, stock);

            product.Should().NotBeNull();
            product.ProductCode.Should().Be(productCode);
            product.Price.Should().BeGreaterThan(decimal.Zero);
            product.Stock.Should().BeGreaterThan(0);
        }

        [TestCase("", 143, 14)]
        [TestCase("OUTHZTG2002", 0, 14)]
        [TestCase("OUTHZTG2002", 143, -2)]
        public void When_Parameters_Incorrect_Should_Throw_Error(string productCode, decimal price, int stock)
        {
            Action act = () => new Product(productCode, price, stock);

            act.Should().Throw<ArgumentException>();
        }

        [Test]
        public void When_Campaign_Applied_Should_Price_Changed()
        {
            var productPrice = 500;
            var priceManipulationLimit = 20;

            var product = new Product("OUTHZTG2002", productPrice, 500);
            var campaign = Campaign.Create(100, priceManipulationLimit, 10, "OUTHZTG2002", "Campaign 4342");

            product.ApplyCampaign(campaign, new PriceCalculationStrategy());

            var newPrice = productPrice * (100 - priceManipulationLimit)/100;
            product.Price.Should().Be(newPrice);
            product.AppliedCampaignId.Should().Be(campaign.CampaignId);
        }

        [Test]
        public void When_Quantity_Gt_Stock_Should_Throw_InSufficientStockException()
        {
            var productStock = 100;
            var product = new Product("OUTHZTG2002", 434, productStock);

            int quantity = 120;

            Action act = () => product.Sell(quantity);

            act.Should().Throw<InSufficientStockException>();
        }

        [Test]
        public void When_Product_Sell_Should_Stock_Decreased()
        {
            var productStock = 500;
            var product = new Product("OUTHZTG2002", 434, productStock);

            int quantity = 120;

            product.Sell(quantity);

            product.Stock.Should().Be(productStock - quantity);
        }
    }
}
