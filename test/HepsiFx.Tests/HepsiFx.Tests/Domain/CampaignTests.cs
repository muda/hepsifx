﻿using NUnit.Framework;
using System;
using FluentAssertions;
using HepsiFx.Domain;

namespace HepsiFx.Tests.Domain
{
    [TestFixture]
    public class CampaignTests :BaseTest
    {
        private IDateTimeProvider _dateTimeProvider;

        [SetUp]
        public override void Setup()
        {
            _dateTimeProvider = new DefaultDateTimeProvider();
            base.Setup();
        }


        [Test()]
        public void When_Parameters_Given_Should_Campaign_Created()
        {
            var campaign = Campaign.Create(180, 5, 5, "OUTFARG2300", "Hede Hodo");

            campaign.Should().NotBeNull();
            campaign.EndTime.Should().BeOnOrAfter(_dateTimeProvider.Now);
        }

        [TestCase(180, 150, -5, "", "Hede Hodo")]
        [TestCase(180, 0, 1, "dawda", "Hede Hodo")]
        [TestCase(180, 1, 1, "dawda", "")]
        [TestCase(0, 1, 1, "dawda", "dawdaw")]
        public void When_Parameters_Incorrect_Should_Throw_Error(int targetSalesCount, int priceManipulationLimit, int duration, string productCode, string name)
        {
            Action act = () => Campaign.Create(targetSalesCount, priceManipulationLimit, duration, productCode, name);
            act.Should().Throw<ArgumentException>();
        }

        [Test]
        public void When_CampaignTime_Increased_Should_Campaign_Extend()
        {
            var campaign = Campaign.Create(180, 5, 5, "OUTFARG2300", "Hede Hodo");
            campaign.IncreaseTime(10);

            var hourDifference = campaign.EndTime - _dateTimeProvider.Now;
            hourDifference.Hours.Should().BeGreaterOrEqualTo(14);
        }


    }
}
